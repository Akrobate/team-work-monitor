'use strict';

const {
    mock,
} = require('sinon');

const {
    AggregationService,
    AutoRefreshDataService,
} = require('../src/services');

describe('autoRefreshData', () => {

    const mocks = {};
    const aggregation_service = AggregationService.getInstance();
    const auto_refresh_data_service = AutoRefreshDataService.getInstance();

    beforeEach(() => {
        mocks.aggregation_service = mock(aggregation_service);
    });

    afterEach(() => {
        mocks.aggregation_service.restore();
    });


    it('Should be able to triggre autoRefreshData', async () => {
        mocks.aggregation_service.expects('getCachedTickets')
            .once()
            .returns(Promise.resolve());
        mocks.aggregation_service.expects('getCachedUpcomingMilestones')
            .once()
            .once(Promise.resolve());

        auto_refresh_data_service.setRefreshIntervalMilis(10);
        auto_refresh_data_service.start();
        await new Promise((resolve) => setTimeout(() => resolve(), 20));
        auto_refresh_data_service.stop();
        mocks.aggregation_service.verify();
    });

});
