'use strict';

const {
    mock,
} = require('sinon');

const {
    AggregationService,
} = require('../src/services');
const {
    CacheRepository,
    AssemblaRepository,
} = require('../src/repositories');
const {
    expect,
} = require('chai');


describe('AggregationService', () => {

    const mocks = {};
    const aggregation_service = AggregationService.getInstance();
    const cache_repository = CacheRepository.getInstance();
    const assembla_repository = AssemblaRepository.getInstance();

    beforeEach(() => {
        cache_repository.clearAllCache();
        mocks.aggregation_service = mock(aggregation_service);
        mocks.cache_repository = mock(cache_repository);
        mocks.assembla_repository = mock(assembla_repository);
    });
    
    afterEach(() => {
        cache_repository.clearAllCache();
        mocks.aggregation_service.restore();
        mocks.cache_repository.restore();
        mocks.assembla_repository.restore();
    });


    it('getTickets check working cache without force', async () => {
        mocks.aggregation_service.expects('getTickets')
            .once()
            .returns(Promise.resolve([{
                a: 1,
            }]));

        const numer_of_pages_to_fetch = 2;
        const force_update_cache = false;

        const data_1 = await aggregation_service.getCachedTickets(
            numer_of_pages_to_fetch,
            force_update_cache
        );

        const data_2 = await aggregation_service.getCachedTickets(
            numer_of_pages_to_fetch,
            force_update_cache
        );

        expect(data_1).to.deep.equal(data_2);
    });


    it('getCachedUpcomingMilestones check working cache without force', async () => {
        mocks.aggregation_service.expects('getUpcomingMilestones')
            .once()
            .returns(Promise.resolve([{
                b: 2,
            }]));

        const force_update_cache = false;

        const data_1 = await aggregation_service
            .getCachedUpcomingMilestones(force_update_cache);

        const data_2 = await aggregation_service
            .getCachedUpcomingMilestones(force_update_cache);

        expect(data_1).to.deep.equal(data_2);
    });



    
    it('getTickets', async () => {
        mocks.assembla_repository.expects('getTickets')
            .once()
            .returns(Promise.resolve([{
                a: 1,
            }]));

        mocks.assembla_repository.expects('getTickets')
            .once()
            .returns(Promise.resolve([{
                b: 2,
            }]));

        const data = await aggregation_service.getTickets(
            'random space id'
        );

        expect(data).to.deep.equal(
            [
                {
                    a: 1,
                },
                {
                    b: 2,
                },
            ]
        )

    });

});
