'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');

const {
    app,
} = require('../src/app');
const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const axios = require('axios');

const {
    AggregationService,
} = require('../src/services/AggregationService');


const superApp = superTest(app);

const {
    configuration,
} = require('../src/configuration');

describe('getMilestones', () => {

    const mocks = {};
    const aggregation_service = AggregationService.getInstance();

    beforeEach(() => {
        mocks.axios = mock(axios);
        mocks.aggregation_service = mock(aggregation_service);
    });

    afterEach(() => {
        mocks.axios.restore();
        mocks.aggregation_service.restore();
    });


    it('Should not be able to get milestone without auth token', async () => {
        await superApp
            .get('/milestones')
            .expect(HTTP_CODE.UNAUTHORIZED);
    });


    it('Should be able to get milestone with auth token', async () => {
        const token = Buffer.from(configuration.ASSEMBLA_CLIENT_SECRET, 'utf8')
            .toString('base64')
            .substr(0, 10);


        mocks.aggregation_service.expects('getCachedUpcomingMilestones')
            .returns(Promise.resolve([]));

        await superApp
            .get('/milestones')
            .set({
                'x-token': token,
            })
            .expect(HTTP_CODE.OK);

        mocks.aggregation_service.verify();
    });


    it('Should be able to get milestone', async () => {
        const token = Buffer.from(configuration.ASSEMBLA_CLIENT_SECRET, 'utf8')
            .toString('base64')
            .substr(0, 10);

        mocks.axios.expects('get')
            .once()
            .returns(Promise.resolve({
                data: [],
            }));

        await superApp
            .get('/milestones')
            .expect(HTTP_CODE.OK)
            .set({
                'x-token': token,
            })
            .expect((response) => {
                expect(response).to.have.property('body');
                mocks.axios.verify();
            });
    });
});
