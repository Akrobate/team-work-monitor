'use strict';

const express = require('express');
const cors = require('cors');
const app = express();

const {
    base_url,
    route_collection,
} = require('./routes');

const {
    checkXTokenAuth,
} = require('./middlewares');

app.disable('x-powered-by');
app.use(cors());
app.use(express.json());
app.use(checkXTokenAuth);
app.use(base_url, route_collection);

module.exports = {
    app,
};
