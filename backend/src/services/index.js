'use strict';

const {
    AggregationService,
} = require('./AggregationService');

const {
    AutoRefreshDataService,
} = require('./AutoRefreshDataService');

module.exports = {
    AggregationService,
    AutoRefreshDataService,
};
