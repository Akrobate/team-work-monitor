'use strict';

const {
    AggregationService,
} = require('./AggregationService');

const {
    configuration,
} = require('../configuration');

class AutoRefreshDataService {

    /**
     * @param {AggregationService} aggregation_service
     */
    constructor(
        aggregation_service
    ) {
        this.aggregation_service = aggregation_service;
        this.refresh_interval_milis = configuration.REFRESH_INTERVAL;
        this.interval_handler = null;
    }


    /* istanbul ignore next */
    /**
     * @returns {AutoRefreshDataService}
     */
    static getInstance() {
        if (AutoRefreshDataService.instance === null) {
            AutoRefreshDataService.instance = new AutoRefreshDataService(
                AggregationService.getInstance()
            );
        }

        return AutoRefreshDataService.instance;
    }


    /**
     * @return {Promise}
     */
    async processRefresh() {
        const force_update_cache = true;
        const numer_of_pages_to_fetch = 2;
        await this.aggregation_service
            .getCachedTickets(numer_of_pages_to_fetch, force_update_cache);
        await this.aggregation_service.getCachedUpcomingMilestones(force_update_cache);
    }


    /**
     * @returns {void}
     */
    start() {
        this.interval_handler = setInterval(
            () => {
                this.processRefresh();
            },
            this.refresh_interval_milis
        );
    }


    /**
     * @returns {void}
     */
    stop() {
        clearInterval(this.interval_handler);
    }


    /**
     * @param {Number} refresh_interval_milis
     * @returns {void}
     */
    setRefreshIntervalMilis(refresh_interval_milis) {
        this.refresh_interval_milis = refresh_interval_milis;
    }

}

AutoRefreshDataService.instance = null;

module.exports = {
    AutoRefreshDataService,
};
