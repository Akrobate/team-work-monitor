'use strict';

const {
    AssemblaRepository,
    CacheRepository,
} = require('../repositories');

const {
    configuration,
} = require('../configuration');

class AggregationService {


    /**
     * @param {AssemblaRepository} assembla_repository
     * @param {CacheRepository} cache_repository
     */
    constructor(
        assembla_repository,
        cache_repository
    ) {
        this.assembla_repository = assembla_repository;
        this.cache_repository = cache_repository;
    }


    /* istanbul ignore next */
    /**
     * @returns {AggregationService}
     */
    static getInstance() {
        if (AggregationService.instance === null) {
            AggregationService.instance = new AggregationService(
                AssemblaRepository.getInstance(),
                CacheRepository.getInstance()
            );
        }
        return AggregationService.instance;
    }


    /**
     * @param {Number} numer_of_pages_to_fetch
     * @param {Boolean} force_update_cache
     * @returns {Promise<Array>}
     */
    async getCachedTickets(numer_of_pages_to_fetch = 2, force_update_cache = false) {
        const space_id = configuration.ASSEMBLA_SPACE_ID;
        const cache_key = `ticket_list_${space_id}_page_${numer_of_pages_to_fetch}`;
        if (!this.cache_repository.getCheckDataAvailable(cache_key) || force_update_cache) {
            const ticket_list = await this.getTickets(space_id, numer_of_pages_to_fetch);
            this.cache_repository.setData(cache_key, ticket_list);
        }
        return this.cache_repository.getData(cache_key);
    }


    /**
     * @param {Boolean} force_update_cache
     * @returns {Promise<Array>}
     */
    async getCachedUpcomingMilestones(force_update_cache = false) {
        const space_id = configuration.ASSEMBLA_SPACE_ID;
        const cache_key = `upcoming_milestones_${space_id}`;
        if (!this.cache_repository.getCheckDataAvailable(cache_key) || force_update_cache) {
            const upcoming_milestone_list = await this.getUpcomingMilestones(space_id);
            this.cache_repository.setData(cache_key, upcoming_milestone_list);
        }
        return this.cache_repository.getData(cache_key);
    }


    /**
     * @param {String} space_id
     * @param {Number} numer_of_pages_to_fetch
     * @returns {Promise}
     */
    async getTickets(space_id, numer_of_pages_to_fetch = 2) {
        let all_ticket_list = [];
        for (let page = 1; page <= numer_of_pages_to_fetch; page++) {
            const data = await this.assembla_repository
                .getTickets({
                    space_id,
                    page,
                    report: null,
                    per_page: 100,
                    sort_by: 'updated_at',
                    sort_order: 'desc',
                });
            all_ticket_list = all_ticket_list.concat(...data);
        }
        return all_ticket_list;
    }


    /**
     * @param {String} space_id
     * @returns {Promise<Array>}
     */
    getUpcomingMilestones(space_id) {
        return this
            .assembla_repository
            .getUpcomingMilestones(space_id, 0, 100);
    }

}

AggregationService.instance = null;

module.exports = {
    AggregationService,
};
