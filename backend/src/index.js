/* istanbul ignore file */

'use strict';

const {
    configuration,
} = require('./configuration');

const {
    app,
} = require('./app');

const {
    AutoRefreshDataService,
} = require('./services');

AutoRefreshDataService.getInstance().start();

app.listen(configuration.SERVER_PORT, () => {
    console.log(`listening on port: ${configuration.SERVER_PORT}`);
});
