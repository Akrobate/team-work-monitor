/* eslint-disable no-process-env */

'use strict';

const configuration = {
    SERVER_PORT: 8085,
    ASSEMBLA_CLIENT_SECRET: process.env.ASSEMBLA_CLIENT_SECRET,
    ASSEMBLA_CLIENT_ID: process.env.ASSEMBLA_CLIENT_ID,
    ASSEMBLA_SPACE_ID: process.env.ASSEMBLA_SPACE_ID,
    REFRESH_INTERVAL: 120000,
    VUE_APP_URL: process.env.VUE_APP_URL,
};

module.exports = {
    configuration,
};
