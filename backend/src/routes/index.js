'use strict';

const {
    Router,
} = require('express');

const {
    AggregationController,
} = require('../controllers');

const route_collection = new Router();
const base_url = '/';

route_collection
    .get(
        '/milestones',
        (request, response, next) => AggregationController
            .getInstance()
            .getCachedUpcomingMilestones(request, response)
            .catch(next)
    );

route_collection
    .get(
        '/tickets',
        (request, response, next) => AggregationController
            .getInstance()
            .getCachedTickets(request, response)
            .catch(next)
    );

module.exports = {
    route_collection,
    base_url,
};
