'use strict';

const {
    AggregationService,
} = require('../services');

const HTTP_CODE = require('http-status');

class AggregationController {


    /**
     * @param {AssemblaRepository} aggregation_service
     */
    constructor(
        aggregation_service
    ) {
        this.aggregation_service = aggregation_service;
    }


    /* istanbul ignore next */
    /**
     * @returns {AggregationController}
     */
    static getInstance() {
        if (AggregationController.instance === null) {
            AggregationController.instance = new AggregationController(
                AggregationService.getInstance()
            );
        }
        return AggregationController.instance;
    }


    /**
     * @param {Request} request
     * @param {Response} response
     * @returns {Array}
     */
    async getCachedTickets(request, response) {
        const ticket_list = await this.aggregation_service.getCachedTickets();
        return response.status(HTTP_CODE.OK).send({
            ticket_list,
        });
    }


    /**
     * @param {Request} request
     * @param {Response} response
     * @returns {Array}
     */
    async getCachedUpcomingMilestones(request, response) {
        const milestone_list = await this.aggregation_service.getCachedUpcomingMilestones();
        return response.status(HTTP_CODE.OK).send({
            milestone_list,
        });
    }

}

AggregationController.instance = null;

module.exports = {
    AggregationController,
};
