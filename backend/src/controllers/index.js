'use strict';

const {
    AggregationController,
} = require('./AggregationController');

module.exports = {
    AggregationController,
};
