'use strict';

const {
    checkXTokenAuth,
} = require('./checkXTokenAuth');

module.exports = {
    checkXTokenAuth,
};
