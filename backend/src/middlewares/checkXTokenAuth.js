'use strict';

const {
    configuration,
} = require('../configuration');

const checkXTokenAuth = (request, response, next) => {
    const x_token = request.headers['x-token'];
    const token_should_be = Buffer.from(configuration.ASSEMBLA_CLIENT_SECRET, 'utf8')
        .toString('base64')
        .substr(0, 10);

    if (x_token !== token_should_be) {
        return response
            .status(401)
            .send({
                message: 'Invalid token',
            });
    }
    return next();
};

module.exports = {
    checkXTokenAuth,
};
