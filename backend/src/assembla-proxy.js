/* eslint-disable no-process-env */
/* istanbul ignore file */

'use strict';

// Configurations
const port = 8085;
const READ_ONLY = true;
const ASSEMBLA_URL = 'https://api.assembla.com';

const express = require('express');
const cors = require('cors');
const axios = require('axios');
const app = express();

app.disable('x-powered-by');
app.use(cors());
app.use(express.json());

app.use(async (request, response) => {

    if (request.headers['x-token']) {
        const x_token = request.headers['x-token'];
        const token_should_be = Buffer.from(process.env.ASSEMBLA_CLIENT_SECRET, 'utf8')
            .toString('base64')
            .substr(0, 10);
        if (x_token !== token_should_be) {
            return response
                .status(401)
                .send({
                    message: 'Invalid token',
                });
        }
    }


    if (READ_ONLY && request.method.toLocaleLowerCase() !== 'get') {
        return response
            .status(401)
            .send({
                message: 'Read only (GET) methods are allowed',
            });
    }

    let assembla_response = null;
    try {
        assembla_response = await axios({
            method: request.method.toLocaleLowerCase(),
            url: `${ASSEMBLA_URL}${request.url}`,
            data: request.body,
            params: request.query,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-type': 'application/json',
                'X-Api-Key': request.headers['x-api-key'] ? request.headers['x-api-key'] : process.env.ASSEMBLA_CLIENT_ID,
                'X-Api-Secret': request.headers['x-api-secret'] ? request.headers['x-api-secret'] : process.env.ASSEMBLA_CLIENT_SECRET,
            },
        });
    } catch (error) {
        const error_data = error.toJSON();
        assembla_response = {
            status: error_data.status,
            data: {
                message: error_data.message,
            },
        };
    }

    return response
        .status(assembla_response.status)
        .send(assembla_response.data);

}).listen(port, () => {
    console.log(`listening on port: ${port}`);
});
