/* istanbul ignore file */

'use strict';


const {
    configuration,
} = require('../configuration');

const {
    ASSEMBLA_CLIENT_SECRET,
    VUE_APP_URL,
} = configuration;

const token = Buffer.from(ASSEMBLA_CLIENT_SECRET, 'utf8')
    .toString('base64')
    .substr(0, 10);

console.log('********************************************************************');
console.log(`Token: ${token}`);
console.log(`Url: ${VUE_APP_URL}?token=${token}`);
console.log('********************************************************************');
