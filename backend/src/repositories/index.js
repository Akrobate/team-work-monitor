'use strict';

const {
    CacheRepository,
} = require('./CacheRepository');
const {
    AssemblaRepository,
} = require('./AssemblaRepository');


module.exports = {
    AssemblaRepository,
    CacheRepository,
};
