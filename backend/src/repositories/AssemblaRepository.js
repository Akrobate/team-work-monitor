'use strict';

const axios = require('axios');

class AssemblaRepository {

    /**
     * constructor
     * @param  {Request} request
     * @returns {AssemblaRepository}
     */
    constructor() {
        this.client_id = null;
        this.client_secret = null;
    }

    /* istanbul ignore next */
    /**
     * @returns {AssemblaRepository}
     */
    static getInstance() {
        if (AssemblaRepository.instance === null) {
            AssemblaRepository.instance = new AssemblaRepository();
        }
        return AssemblaRepository.instance;
    }


    /**
     * @static
     * @retunrs {Number}
     */
    static get TIMEOUT() {
        return 60000;
    }


    /**
     * @static
     * @returns {Object}
     */
    static get REQUEST_HEADERS() {
        return {
            'Access-Control-Allow-Origin': '*',
            Accept: 'application/json',
            'Content-type': 'application/json',
            'X-Api-Key': this.client_id,
            'X-Api-Secret': this.client_secret,
        };
    }


    /**
     * @static
     * @returns {Object}
     */
    static get REQUEST_GENERIC_PARAMS() {
        return {
            headers: AssemblaRepository.REQUEST_HEADERS,
            timeout: AssemblaRepository.TIMEOUT,
        };
    }


    /**
     * @static
     * @retunrs {Object}
     */
    static get ASSEMBLA_URL() {
        return 'https://api.assembla.com';
    }


    /* istanbul ignore next */
    /**
     * @param {String} client_id
     * @returns {void}
     */
    setClientId(client_id) {
        this.client_id = client_id;
    }


    /* istanbul ignore next */
    /**
     * @param {String} client_secret
     * @returns {void}
     */
    setClientSecret(client_secret) {
        this.client_secret = client_secret;
    }


    /* istanbul ignore next */
    /**
     * getSpaces
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getSpaces() {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces.json`,
            {
                ...AssemblaRepository.REQUEST_GENERIC_PARAMS,
            }
        );
        return response.data;
    }


    /**
     * [getAllMilestones description]
     * @param  {String} space_id
     * @param  {[type]} page       [description]
     * @param  {[type]} per_page   [description]
     * @param  {[type]} due_date_order    [description]
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getUpcomingMilestones(space_id, page, per_page, due_date_order) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/milestones/upcoming.json`,
            {
                ...AssemblaRepository.REQUEST_GENERIC_PARAMS,
                params: {
                    page,
                    per_page,
                    due_date_order,
                },
            }
        );
        return response.data;
    }


    /**
     * [getTickets description]
     * @param  {String} space_id
     * @param  {Number} report
     * @param  {[type]} page       [description]
     * @param  {[type]} per_page   [description]
     * @param  {[type]} sort_order [description]
     * @param  {[type]} sort_by    [description]
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getTickets(space_id, report, page, per_page, sort_order, sort_by) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets.json`,
            {
                ...AssemblaRepository.REQUEST_GENERIC_PARAMS,
                params: {
                    report,
                    page,
                    per_page,
                    sort_order,
                    sort_by,
                },
            }
        );
        return response.data;
    }
}

AssemblaRepository.instance = null;

module.exports = {
    AssemblaRepository,
};
