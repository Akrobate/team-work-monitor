'use strict';


class CacheRepository {

    /**
     * @return {CacheRepository}
     */
    constructor() {
        this.data = {};
    }


    /* istanbul ignore next */
    /**
     * @return {CacheRepository}
     */
    static getInstance() {
        if (CacheRepository.instance === null) {
            CacheRepository.instance = new CacheRepository();
        }
        return CacheRepository.instance;
    }


    /**
     * @param {String} key
     * @param {String} data
     * @returns {Void}
     */
    setData(key, data) {
        this.data[key] = data;
    }


    /**
     * @param {String} key
     * @returns {Boolean}
     */
    getCheckDataAvailable(key) {
        return this.data[key] !== undefined;
    }


    /**
     * @param {String} key
     * @returns {String}
     */
    getData(key) {
        return this.data[key];
    }


    /**
     * @param {String} key
     * @returns {String}
     */
    clearAllCache() {
        this.data = {};
    }

}

CacheRepository.instance = null;

module.exports = {
    CacheRepository,
};
