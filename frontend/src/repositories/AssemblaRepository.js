'use strict';

const axios = require('axios')
import { configuration } from '@/configurations'

class AssemblaRepository {

    /**
     * @static
     * @retunrs {Number}
     */
    static get TIMEOUT() {
        return 60000;
    }


    /**
     * @static
     * @retunrs {Object}
     */
    static get ASSEMBLA_URL() {
        return configuration.ASSEMBLA_URL
    }


    /**
     * @static
     * @retunrs {Object}
     */
    REQUEST_HEADERS() {
        return {
            'Access-Control-Allow-Origin': '*',
            Accept: 'application/json',
            'Content-type': 'application/json',
            // 'X-Api-Key': this.client_id,
            // 'X-Api-Secret': this.client_secret,
            'X-token': this.token,
        };
    }

    /**
     * @static
     * @retunrs {Object}
     */
    REQUEST_GENERIC_PARAMS() {
        return {
            headers: this.REQUEST_HEADERS(),
            // timeout: AssemblaRepository.TIMEOUT,
        };
    }

    /**
     * getInstance instance getter
     * @param  {String} token_access       Authentication token
     * @return {AssemblaRepository}
     */
    static getInstance() {
        if (AssemblaRepository.instance === null) {
            AssemblaRepository.instance = new AssemblaRepository()
        }
        return AssemblaRepository.instance
    }

    /**
     * constructor
     * @param  {Request} request
     * @returns {AssemblaRepository}
     */
    constructor() {
        this.client_id = null
        this.client_secret = null
        this.token = null
    }


    /**
     * @param {String} client_id
     * @returns {void}
     */
    setClientId(client_id) {
        this.client_id = client_id
    }


    /**
     * @param {String} client_id
     * @returns {void}
     */
    setToken(token) {
        this.token = token
    }


    /**
     * @param {String} client_id
     * @returns {void}
     */
    setClientSecret(client_secret) {
        this.client_secret = client_secret
    }


    /**
     * getMe return the connected user information
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getMe() {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/user.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
            }
        )
        return response.data
    }


    /**
     * getSpaces
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getSpaces() {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
            }            
        )
        return response.data
    }


    /**
     * [getTickets description]
     * @param  {String} space_id
     * @param  {Number} report
     * @param  {[type]} page       [description]
     * @param  {[type]} per_page   [description]
     * @param  {[type]} sort_order [description]
     * @param  {[type]} sort_by    [description]
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getTickets(space_id, report, page, per_page, sort_order, sort_by) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
                params: {
                    report,
                    page,
                    per_page,
                    sort_order,
                    sort_by,
                },
            }
        )
        return response.data
    }


    /**
     * Returns ticket by ticket number
     *
     * GET /v1/spaces/:space_id/tickets/:number
     *
     * @param  {String} space_id
     * @param  {Number} ticket_number
     * @return {Promise<Object>}
     */
    async getTicket(space_id, ticket_number) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/${ticket_number}`,
            {
                ...this.REQUEST_GENERIC_PARAMS()
            }
        )
        return response.data
    }


    /**
     * [getAllMilestones description]
     * @param  {String} space_id
     * @param  {[type]} page       [description]
     * @param  {[type]} per_page   [description]
     * @param  {[type]} due_date_order    [description]
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getAllMilestones(space_id, page, per_page, due_date_order) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/milestones/all.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
                params: {
                    page,
                    per_page,
                    due_date_order,
                },
            }
        )
        return response.data
    }


    /**
     * [getAllMilestones description]
     * @param  {String} space_id
     * @param  {[type]} page       [description]
     * @param  {[type]} per_page   [description]
     * @param  {[type]} due_date_order    [description]
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getUpcomingMilestones(space_id, page, per_page, due_date_order) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/milestones/upcoming.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
                params: {
                    page,
                    per_page,
                    due_date_order,
                },
            }
        )
        return response.data
    }


    /**
     * @param  {String} space_id
     * @param  {Number} page
     * @param  {Number} per_page
     * @return {Promise<Object>}
     */
    async getTags(space_id, page, per_page) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tags.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
                params: {
                    page,
                    per_page,
                },
            }
        )
        return response.data
    }


    /**
     * @param {String} milestone_id
     * @param {String} space_id
     * @param {String} ticket_status ('active', 'closed' and 'all'. By default, 'active' tickets are fetched)
     * @param {Number} page
     * @param {Number} per_page
     * @param {*} sort_order
     * @param {*} sort_by
     * @returns {Promise<Object>}
     */
    async getTicketsByMilestone(milestone_id, space_id, ticket_status, page, per_page, sort_order, sort_by) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/milestone/${milestone_id}.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
                params: {
                    ticket_status,
                    page,
                    per_page,
                    sort_order,
                    sort_by,
                },
            }
        )
        return response.data
    }


    /**
     * @param {String} space_id
     * @param {Number} ticket_number
     * @returns {Promise<Object>}
     */
    async getTagsByTicketNumber(space_id, ticket_number) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/${ticket_number}/tags.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
            }
        )
        return response.data
    }


    /**
     * Returns all available statuses
     * @param {String} space_id
     * @returns {Promise}
     */
    async getTicketStatusesBySpaceId(space_id) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/statuses.json`,
            {
                ...this.REQUEST_GENERIC_PARAMS(),
            }
        )
        return response.data
    }

}

AssemblaRepository.instance = null

const assembla_repository = AssemblaRepository.getInstance()

export {
    AssemblaRepository,
    assembla_repository,
};
