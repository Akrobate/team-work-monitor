import Vue from 'vue'
import VueRouter from 'vue-router'
import MainDisplay from '@/components/MainDisplay.vue'
import MobileDisplay from '@/components/MobileDisplay.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'main',
    component: MainDisplay
  },
  {
    path: '/mobile',
    name: 'mobile',
    component: MobileDisplay
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
