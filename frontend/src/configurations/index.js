'use strict'

const configuration = {
    ASSEMBLA_URL: process.env.VUE_APP_ASSEMBLA_URL,
    URL: process.env.VUE_APP_URL,
    ASSAMBLA_SPACE_ID: process.env.VUE_APP_ASSEMBLA_SPACE_ID,
};

export {
    configuration,
}
