import Vue from 'vue'
import Vuex from 'vuex'

import { assembla_repository } from '@/repositories/AssemblaRepository'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        client_id: null,
        client_secret: null,
        token: null,
        space_id: null,

        ticket_list: [],
        milestone_list: [],
    },
    getters: {
        ticketList: (state) => state.ticket_list
            .filter((ticket) => state.milestone_list.map((milestone) => milestone.id).includes(ticket.milestone_id))
            .map((ticket) => ({
                ...ticket,
                milestone_name: state.milestone_list.find((milestone) => milestone.id == ticket.milestone_id).title,
            })),
        currentMilestone: (state) => state.milestone_list
            .find((milestone) => milestone.planner_type === 2),
        getToken: (state) => state.token,
    },
    actions: {
        async init({ commit, dispatch }, data) {
            const {
                token,
                space_id,
            } = data         
            commit('set_token', token)            
            assembla_repository.setToken(token)            
            const response = await Promise.all([
                dispatch('getUpcomingMilestones', {
                    space_id,
                    per_page: 100,
                }),
                dispatch('getTicketList', { space_id })
            ])
            const [
                milestone_list,
                ticket_list,
            ] = response
 
            commit('set_milestone_list', milestone_list)
            const prepared_ticket_list = ticket_list
                .filter((ticket) => milestone_list.map((milestone) => milestone.id).includes(ticket.milestone_id))
                .map((ticket) => ({
                    ...ticket,
                    milestone_name: milestone_list.find((milestone) => milestone.id == ticket.milestone_id).title,
                }))
            commit('set_ticket_list', prepared_ticket_list)
        },

        getSpaces() {
            return assembla_repository.getSpaces()
        },

        async getTicketList({ dispatch }, { space_id }) {
            let all_ticket_list = [];
            for (const page of [1, 2]) {
                const data = await dispatch(
                    'getTickets',
                    {
                        space_id,
                        page,
                        report: null,
                        per_page: 100,
                        sort_by: 'updated_at',
                        sort_order: 'desc',
                    }
                )
                all_ticket_list = all_ticket_list.concat(...data)
            }
            return all_ticket_list;
        },

        getTickets(_, { space_id, report, page, per_page, sort_order, sort_by }) {
            return assembla_repository.getTickets(space_id, report, page, per_page, sort_order, sort_by)
        },

        getUpcomingMilestones(_, { space_id, page, per_page, due_date_order }) {
            return assembla_repository.getUpcomingMilestones(space_id, page, per_page, due_date_order)
        },

        getTagsByTicketNumber(_, { space_id, ticket_number }) {
            return assembla_repository.getTagsByTicketNumber(space_id, ticket_number)
        }
    },
    mutations: {
        set_client_id(state, client_id) {
            state.client_id = client_id
        },
        set_client_secret(state, client_secret) {
            state.client_secret = client_secret
        },
        set_ticket_list(state, ticket_list) {
            state.ticket_list = ticket_list
        },
        set_milestone_list(state, milestone_list) {
            state.milestone_list = milestone_list
        },
        set_space_id(state, space_id) {
            state.space_id = space_id
        },
        set_token(state, token) {
            state.token = token
        },
    },
})
