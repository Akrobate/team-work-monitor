'use strict'

const CUSTOM_COLORS = {
    YELLOW: 'yellow',
    WHITE: 'white',
    RED: '#ff0000',
    VIOLET: '#784789',
    GREEN: '#79d93a',
    BLUE: '#5e81f4',
}


const STATUS = {
    IN_PROGRESS: {
        name: 'In Progress',
        icon: 'mdi-hammer-wrench',
        background_color: CUSTOM_COLORS.RED,
        color: CUSTOM_COLORS.YELLOW,
        translation: 'en développement',
    },
    PR: {
        name: 'Technical Review (PR)',
        icon: 'mdi-glasses',
        color: CUSTOM_COLORS.WHITE,
        background_color: CUSTOM_COLORS.VIOLET,
        translation: 'relecture technique',
    },
    DEPLOYED: {
        name: 'Deployed',
        icon: 'mdi-cloud-check-outline',
        color: CUSTOM_COLORS.WHITE,
        background_color: CUSTOM_COLORS.GREEN,
        translation: 'déployé en production',
    },
    TECH_DONE: {
        name: 'Technical Done',
        icon: 'mdi-progress-wrench',
        translation: 'développement terminé',
    },
    DONE: {
        name: 'Done',
        icon: 'mdi-check-outline',
        color: CUSTOM_COLORS.WHITE,
        background_color: CUSTOM_COLORS.GREEN,
        translation: 'prêt pour la prod',
    },
    TO_TEST: {
        name: 'Merged (To test)',
        icon: 'mdi-account-supervisor',
        color: CUSTOM_COLORS.WHITE,
        background_color: CUSTOM_COLORS.VIOLET,
        translation: 'en recette MOA',
    },
    OTHERS: {
        name: null,
        icon: 'mdi-star',
    }
};

const COLOR = {
    BLUE: CUSTOM_COLORS.BLUE,
    DEFAULT_ICON: CUSTOM_COLORS.WHITE,
    DEFAULT_ICON_BACKGROUND: CUSTOM_COLORS.BLUE,
}

const STATUS_LIST = [
    STATUS.IN_PROGRESS,
    STATUS.PR,
    STATUS.DEPLOYED,
    STATUS.TECH_DONE,
    STATUS.DONE,
    STATUS.TO_TEST,
];

export {
    COLOR,
    STATUS,
    STATUS_LIST,
}