module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    proxy: {
      "^/api-assembla": {
        target: "https://api.assembla.com",
        changeOrigin: true,
        logLevel: "debug",
        pathRewrite: { "^/api-assembla": "/" }
      }
    }
  },
}
